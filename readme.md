# Instructions:
 * Project is in impl directory.
 * HighLevel Layed approch i adopted is RestController, Service, Proxy
 * Proxy: Handles communication outside of current project.(This layer is awayre of all endpoints of respective API)
 * Controller: Its a communication layer and tranform input and outputs to appropriate DTOs, it also handle the Content type (Consume/Produce)( JSON, XML, HTML)
   all mappings are defined here
 * Service: It is a business layer, where all logics and intelligence of the business reside, business specific validations, decision to choose channel (Proxy, DAO) all reside here, it also transform beans to respective DTOs
    * It acts like Façade for (one window operation)
    * ACL: Access control list is applied on this layer which make sure any Controller(public api /private api) will be authorized
    * Another service can call a Service
    * A consumber(queue) can call a Service
    * A timer service can call a Service
 * DAO: All datbase logic and table mappings reside here
 * Controller is for Handling API Request as mentioned.
 * Service is a business Layer, where all logics and intelligence of the business lie.
 * ResponseFilterFactory is responsible for creating MovieResponseFilter Object based on filter value. To start with i implemented only CensoringResponseFilter.
 * This pattern will help us to implement any future FilterType without changing much code. You just need to update factory how the newer filter type will be handled.
 * In Unit Tests API response is mocked and all my unit cases are based on mocked Object.
 * Unit test cases are written at Service Layer.
 * Have written 4 Unit tests which are Covering tabular requirement explained in Java Developer Code Assignment.
 * Used RestTemplate and API Stubs are generated using an online utility.

# How to run Development Assignment
 * navigate to impl directory using a shell
 * execute mvn spring-boot:run
 * this will run assignment project on port 8080. Please make sure port 8080 is free otherwise project fails to run.
 * to execute test cases run mvn clean test command.

# Test URLs
 * http://localhost:8080/media?filter=censoring&level=censored 
 * http://localhost:8080/media?filter=censoring&level=uncensored
 * http://localhost:8080/media?filter=censoring&level=null
 * http://localhost:8080/media?filter=censoring&level=
 * http://localhost:8080/media?filter=censoring
 * http://localhost:8080/media
 
# Framework/Libraries Used
* Spring Boot
* Mocktio 
* Junit
* Lomback (https://projectlombok.org/)

# Assumptions
* JDK, maven are ready installed and properly configured on system you wish to run this.
* You system needs an internet connection to download dependencies.
* I have generated Proxy Pojo Classes using an Online Utility. Please ignore any class hierarchy generated for these Pojo. as manually generating them
  will take lot of human efforts. 
* if level parameter is missing from request or it is null, then i am assuming i need to pick entries with contentClassification as empty/null OR Uncensored.
* If no filter is provided censoring will be used as default fallback.
* External API is up all the time and i hav't implement any circuit breaker. In realtime i will definatly recommand to implement a fallback method incase other service is down.