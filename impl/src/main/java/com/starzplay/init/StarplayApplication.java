package com.starzplay.init;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
/**
 * 
 * 
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.init
 * @project starzplay
 *
 */
@SpringBootApplication(scanBasePackages= {"com.starzplay"})
public class StarplayApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarplayApplication.class, args);
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
}
