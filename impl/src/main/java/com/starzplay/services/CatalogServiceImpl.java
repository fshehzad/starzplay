package com.starzplay.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.starzplay.dto.autogen.MovieResponse;
import com.starzplay.factory.MovieResponseFilter;
import com.starzplay.factory.ResponseFilterFactory;
import com.starzplay.proxy.CatalogProxy;
import com.starzplay.web.request.MediaRequest;

import lombok.extern.log4j.Log4j;

@Log4j
@Service("catalogService")
/**
 * 
 * 
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.services
 * @project starzplay
 *
 */
public class CatalogServiceImpl implements CatalogService {

	@Autowired
	private CatalogProxy catalogProxy;
	
	
	@Override
	public MovieResponse process(MediaRequest form) {
		MovieResponse response = catalogProxy.fetch();
		MovieResponseFilter filterFactory = ResponseFilterFactory.getInstance(form.getFilter());
		if(null == filterFactory) {
			return response;
		}
		return filterFactory.process(form.getLevel(), response);
	}

}
