package com.starzplay.services;

import com.starzplay.dto.autogen.MovieResponse;
import com.starzplay.web.request.MediaRequest;
/**
 * 
 * 
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.services
 * @project starzplay
 *
 */
public interface CatalogService {
	
	/**
	 * 
	 * @param form
	 * @return
	 */
	public MovieResponse process( MediaRequest form);

}
