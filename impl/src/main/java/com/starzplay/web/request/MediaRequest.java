package com.starzplay.web.request;

import lombok.Getter;
/**
 * 
 * 
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.web.request
 * @project starzplay
 *
 */
@Getter
public class MediaRequest{
	
	private String filter;
	private String level;
	
	/**
	 * 
	 * @param filter
	 * @param level
	 */
	public MediaRequest(String filter, String level) {
		this.filter = "censoring";
		if(null != level && ( level.equals("uncensored") || level.equals("censored")) ) {
			this.level = level;	
		}
	}
}
