package com.starzplay.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.starzplay.dto.autogen.Media;
import com.starzplay.dto.autogen.MovieResponse;
import com.starzplay.services.CatalogService;
import com.starzplay.web.request.MediaRequest;

import lombok.extern.log4j.Log4j;

@RestController()
@RequestMapping("/media")
@Log4j
/**
 * 
 * 
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.web.controllers
 * @project starzplay
 *
 */
public class MediaController {
	
	@Autowired
	private CatalogService catalogService;
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public MovieResponse index(@RequestParam(name="filter", defaultValue="censoring", required=true) String filter, 
			@RequestParam(name="level", defaultValue="", required=false) String level) {
			
		log.info(" LEVEL IS  :: " + level);
		MediaRequest form = new MediaRequest(filter, level);
		return catalogService.process(form);
	}
	
}
