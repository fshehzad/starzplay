
package com.starzplay.dto.autogen;

import java.util.HashMap;
import java.util.Map;

public class Tag {

    private String scheme;
    private String title;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
