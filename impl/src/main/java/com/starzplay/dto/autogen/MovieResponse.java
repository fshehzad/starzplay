
package com.starzplay.dto.autogen;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MovieResponse {

    private com.starzplay.dto.autogen.$xmlns $xmlns;
    private Integer startIndex;
    private Integer itemsPerPage;
    private Integer entryCount;
    private String title;
    private List<Entry> entries = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public com.starzplay.dto.autogen.$xmlns get$xmlns() {
        return $xmlns;
    }

    public void set$xmlns(com.starzplay.dto.autogen.$xmlns $xmlns) {
        this.$xmlns = $xmlns;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    public Integer getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public Integer getEntryCount() {
        return entryCount;
    }

    public void setEntryCount(Integer entryCount) {
        this.entryCount = entryCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
