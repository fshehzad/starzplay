package com.starzplay.dto.autogen;

import java.io.Serializable;

import lombok.Data;

@Data
public class Media implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private int id;
	private String name;
	
}
