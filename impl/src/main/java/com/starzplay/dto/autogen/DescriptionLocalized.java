
package com.starzplay.dto.autogen;

import java.util.HashMap;
import java.util.Map;

public class DescriptionLocalized {

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
