
package com.starzplay.dto.autogen;

import java.util.HashMap;
import java.util.Map;

public class ShortDescriptionLocalized {

    private String ar;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getAr() {
        return ar;
    }

    public void setAr(String ar) {
        this.ar = ar;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
