
package com.starzplay.dto.autogen;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Entry {

    private String id;
    private String guid;
    private Long updated;
    private String title;
    private Object description;
    private Long added;
    private Boolean approved;
    private List<Credit> credits = null;
    private DescriptionLocalized descriptionLocalized;
    private Object displayGenre;
    private Object editorialRating;
    private List<ImageMediaId> imageMediaIds = null;
    private Object isAdult;
    private List<String> languages = null;
    private String longDescription;
    private LongDescriptionLocalized longDescriptionLocalized;
    private String programType;
    private List<Object> ratings = null;
    private Object seriesEpisodeNumber;
    private Object seriesId;
    private String shortDescription;
    private ShortDescriptionLocalized shortDescriptionLocalized;
    private List<String> tagIds = null;
    private List<Tag> tags = null;
    private Thumbnails thumbnails;
    private TitleLocalized titleLocalized;
    private Object tvSeasonEpisodeNumber;
    private Object tvSeasonId;
    private Object tvSeasonNumber;
    private Integer year;
    private List<Medium> media = null;
    private String peg$ExclusiveFrench;
    private Integer peg$arAgeRating;
    private String peg$arContentRating;
    private String peg$availableInSection;
    private String peg$contentClassification;
    private String peg$contractName;
    private String peg$countryOfOrigin;
    private String peg$priorityLevel;
    private String peg$priorityLevelActionandAdventure;
    private String peg$priorityLevelArabic;
    private String peg$priorityLevelChildrenandFamily;
    private String peg$priorityLevelComedy;
    private String peg$priorityLevelDisney;
    private String peg$priorityLevelDisneyKids;
    private String peg$priorityLevelDrama;
    private String peg$priorityLevelKids;
    private String peg$priorityLevelKidsAction;
    private String peg$priorityLevelKidsFunandAdventure;
    private String peg$priorityLevelKidsMagicandDreams;
    private String peg$priorityLevelKidsPreschool;
    private String peg$priorityLevelRomance;
    private String peg$priorityLevelThriller;
    private String peg$productCode;
    private String peg$programMediaAvailability;
    private String peg$testDefaultValue;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Long getAdded() {
        return added;
    }

    public void setAdded(Long added) {
        this.added = added;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public List<Credit> getCredits() {
        return credits;
    }

    public void setCredits(List<Credit> credits) {
        this.credits = credits;
    }

    public DescriptionLocalized getDescriptionLocalized() {
        return descriptionLocalized;
    }

    public void setDescriptionLocalized(DescriptionLocalized descriptionLocalized) {
        this.descriptionLocalized = descriptionLocalized;
    }

    public Object getDisplayGenre() {
        return displayGenre;
    }

    public void setDisplayGenre(Object displayGenre) {
        this.displayGenre = displayGenre;
    }

    public Object getEditorialRating() {
        return editorialRating;
    }

    public void setEditorialRating(Object editorialRating) {
        this.editorialRating = editorialRating;
    }

    public List<ImageMediaId> getImageMediaIds() {
        return imageMediaIds;
    }

    public void setImageMediaIds(List<ImageMediaId> imageMediaIds) {
        this.imageMediaIds = imageMediaIds;
    }

    public Object getIsAdult() {
        return isAdult;
    }

    public void setIsAdult(Object isAdult) {
        this.isAdult = isAdult;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public LongDescriptionLocalized getLongDescriptionLocalized() {
        return longDescriptionLocalized;
    }

    public void setLongDescriptionLocalized(LongDescriptionLocalized longDescriptionLocalized) {
        this.longDescriptionLocalized = longDescriptionLocalized;
    }

    public String getProgramType() {
        return programType;
    }

    public void setProgramType(String programType) {
        this.programType = programType;
    }

    public List<Object> getRatings() {
        return ratings;
    }

    public void setRatings(List<Object> ratings) {
        this.ratings = ratings;
    }

    public Object getSeriesEpisodeNumber() {
        return seriesEpisodeNumber;
    }

    public void setSeriesEpisodeNumber(Object seriesEpisodeNumber) {
        this.seriesEpisodeNumber = seriesEpisodeNumber;
    }

    public Object getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(Object seriesId) {
        this.seriesId = seriesId;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public ShortDescriptionLocalized getShortDescriptionLocalized() {
        return shortDescriptionLocalized;
    }

    public void setShortDescriptionLocalized(ShortDescriptionLocalized shortDescriptionLocalized) {
        this.shortDescriptionLocalized = shortDescriptionLocalized;
    }

    public List<String> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<String> tagIds) {
        this.tagIds = tagIds;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Thumbnails getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(Thumbnails thumbnails) {
        this.thumbnails = thumbnails;
    }

    public TitleLocalized getTitleLocalized() {
        return titleLocalized;
    }

    public void setTitleLocalized(TitleLocalized titleLocalized) {
        this.titleLocalized = titleLocalized;
    }

    public Object getTvSeasonEpisodeNumber() {
        return tvSeasonEpisodeNumber;
    }

    public void setTvSeasonEpisodeNumber(Object tvSeasonEpisodeNumber) {
        this.tvSeasonEpisodeNumber = tvSeasonEpisodeNumber;
    }

    public Object getTvSeasonId() {
        return tvSeasonId;
    }

    public void setTvSeasonId(Object tvSeasonId) {
        this.tvSeasonId = tvSeasonId;
    }

    public Object getTvSeasonNumber() {
        return tvSeasonNumber;
    }

    public void setTvSeasonNumber(Object tvSeasonNumber) {
        this.tvSeasonNumber = tvSeasonNumber;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public List<Medium> getMedia() {
        return media;
    }

    public void setMedia(List<Medium> media) {
        this.media = media;
    }

    public String getPeg$ExclusiveFrench() {
        return peg$ExclusiveFrench;
    }

    public void setPeg$ExclusiveFrench(String peg$ExclusiveFrench) {
        this.peg$ExclusiveFrench = peg$ExclusiveFrench;
    }

    public Integer getPeg$arAgeRating() {
        return peg$arAgeRating;
    }

    public void setPeg$arAgeRating(Integer peg$arAgeRating) {
        this.peg$arAgeRating = peg$arAgeRating;
    }

    public String getPeg$arContentRating() {
        return peg$arContentRating;
    }

    public void setPeg$arContentRating(String peg$arContentRating) {
        this.peg$arContentRating = peg$arContentRating;
    }

    public String getPeg$availableInSection() {
        return peg$availableInSection;
    }

    public void setPeg$availableInSection(String peg$availableInSection) {
        this.peg$availableInSection = peg$availableInSection;
    }

    public String getPeg$contentClassification() {
        return peg$contentClassification;
    }

    public void setPeg$contentClassification(String peg$contentClassification) {
        this.peg$contentClassification = peg$contentClassification;
    }

    public String getPeg$contractName() {
        return peg$contractName;
    }

    public void setPeg$contractName(String peg$contractName) {
        this.peg$contractName = peg$contractName;
    }

    public String getPeg$countryOfOrigin() {
        return peg$countryOfOrigin;
    }

    public void setPeg$countryOfOrigin(String peg$countryOfOrigin) {
        this.peg$countryOfOrigin = peg$countryOfOrigin;
    }

    public String getPeg$priorityLevel() {
        return peg$priorityLevel;
    }

    public void setPeg$priorityLevel(String peg$priorityLevel) {
        this.peg$priorityLevel = peg$priorityLevel;
    }

    public String getPeg$priorityLevelActionandAdventure() {
        return peg$priorityLevelActionandAdventure;
    }

    public void setPeg$priorityLevelActionandAdventure(String peg$priorityLevelActionandAdventure) {
        this.peg$priorityLevelActionandAdventure = peg$priorityLevelActionandAdventure;
    }

    public String getPeg$priorityLevelArabic() {
        return peg$priorityLevelArabic;
    }

    public void setPeg$priorityLevelArabic(String peg$priorityLevelArabic) {
        this.peg$priorityLevelArabic = peg$priorityLevelArabic;
    }

    public String getPeg$priorityLevelChildrenandFamily() {
        return peg$priorityLevelChildrenandFamily;
    }

    public void setPeg$priorityLevelChildrenandFamily(String peg$priorityLevelChildrenandFamily) {
        this.peg$priorityLevelChildrenandFamily = peg$priorityLevelChildrenandFamily;
    }

    public String getPeg$priorityLevelComedy() {
        return peg$priorityLevelComedy;
    }

    public void setPeg$priorityLevelComedy(String peg$priorityLevelComedy) {
        this.peg$priorityLevelComedy = peg$priorityLevelComedy;
    }

    public String getPeg$priorityLevelDisney() {
        return peg$priorityLevelDisney;
    }

    public void setPeg$priorityLevelDisney(String peg$priorityLevelDisney) {
        this.peg$priorityLevelDisney = peg$priorityLevelDisney;
    }

    public String getPeg$priorityLevelDisneyKids() {
        return peg$priorityLevelDisneyKids;
    }

    public void setPeg$priorityLevelDisneyKids(String peg$priorityLevelDisneyKids) {
        this.peg$priorityLevelDisneyKids = peg$priorityLevelDisneyKids;
    }

    public String getPeg$priorityLevelDrama() {
        return peg$priorityLevelDrama;
    }

    public void setPeg$priorityLevelDrama(String peg$priorityLevelDrama) {
        this.peg$priorityLevelDrama = peg$priorityLevelDrama;
    }

    public String getPeg$priorityLevelKids() {
        return peg$priorityLevelKids;
    }

    public void setPeg$priorityLevelKids(String peg$priorityLevelKids) {
        this.peg$priorityLevelKids = peg$priorityLevelKids;
    }

    public String getPeg$priorityLevelKidsAction() {
        return peg$priorityLevelKidsAction;
    }

    public void setPeg$priorityLevelKidsAction(String peg$priorityLevelKidsAction) {
        this.peg$priorityLevelKidsAction = peg$priorityLevelKidsAction;
    }

    public String getPeg$priorityLevelKidsFunandAdventure() {
        return peg$priorityLevelKidsFunandAdventure;
    }

    public void setPeg$priorityLevelKidsFunandAdventure(String peg$priorityLevelKidsFunandAdventure) {
        this.peg$priorityLevelKidsFunandAdventure = peg$priorityLevelKidsFunandAdventure;
    }

    public String getPeg$priorityLevelKidsMagicandDreams() {
        return peg$priorityLevelKidsMagicandDreams;
    }

    public void setPeg$priorityLevelKidsMagicandDreams(String peg$priorityLevelKidsMagicandDreams) {
        this.peg$priorityLevelKidsMagicandDreams = peg$priorityLevelKidsMagicandDreams;
    }

    public String getPeg$priorityLevelKidsPreschool() {
        return peg$priorityLevelKidsPreschool;
    }

    public void setPeg$priorityLevelKidsPreschool(String peg$priorityLevelKidsPreschool) {
        this.peg$priorityLevelKidsPreschool = peg$priorityLevelKidsPreschool;
    }

    public String getPeg$priorityLevelRomance() {
        return peg$priorityLevelRomance;
    }

    public void setPeg$priorityLevelRomance(String peg$priorityLevelRomance) {
        this.peg$priorityLevelRomance = peg$priorityLevelRomance;
    }

    public String getPeg$priorityLevelThriller() {
        return peg$priorityLevelThriller;
    }

    public void setPeg$priorityLevelThriller(String peg$priorityLevelThriller) {
        this.peg$priorityLevelThriller = peg$priorityLevelThriller;
    }

    public String getPeg$productCode() {
        return peg$productCode;
    }

    public void setPeg$productCode(String peg$productCode) {
        this.peg$productCode = peg$productCode;
    }

    public String getPeg$programMediaAvailability() {
        return peg$programMediaAvailability;
    }

    public void setPeg$programMediaAvailability(String peg$programMediaAvailability) {
        this.peg$programMediaAvailability = peg$programMediaAvailability;
    }

    public String getPeg$testDefaultValue() {
        return peg$testDefaultValue;
    }

    public void setPeg$testDefaultValue(String peg$testDefaultValue) {
        this.peg$testDefaultValue = peg$testDefaultValue;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
