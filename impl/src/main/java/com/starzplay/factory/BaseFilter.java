package com.starzplay.factory;

import com.starzplay.dto.autogen.MovieResponse;

import lombok.Data;

/**
 * 
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.factory
 * @project starzplay
 *
 */
@Data
public abstract class BaseFilter {
	
	private MovieResponse movies;

}
