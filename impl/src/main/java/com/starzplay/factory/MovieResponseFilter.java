package com.starzplay.factory;

import com.starzplay.dto.autogen.MovieResponse;

/**
 * 
 * 
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.factory
 * @project starzplay
 *
 */
public interface MovieResponseFilter {

	/**
	 * 
	 * @param level
	 * @return
	 */
	public MovieResponse process(String level, MovieResponse movies);
}
