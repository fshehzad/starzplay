/**
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.factory
 * @project starplay
 *
 */
package com.starzplay.factory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import com.starzplay.dto.autogen.Entry;
import com.starzplay.dto.autogen.Medium;
import com.starzplay.dto.autogen.MovieResponse;

import lombok.Data;
import lombok.extern.log4j.Log4j;

@Data
@Log4j
@Service("censoringFilter")
/**
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.factory
 * @project starzplay
 *
 */
public class CensoringResponseFilter implements MovieResponseFilter{

	@Override
	public MovieResponse process(String level, MovieResponse movies) {
		return doFilter(level, movies);
	}

	/**
	 * 
	 * @param level
	 * @param response
	 * @return
	 */
	private MovieResponse doFilter(String level, MovieResponse response) {
		
		Iterator<Entry> itr = response.getEntries().iterator();
		boolean processMedia = true;
		if(null == level || level.isEmpty()) {
			processMedia = false;
		}
		
		while(itr.hasNext()) {
			Entry entry = itr.next();
			log.info("Level asked is :: " + level + " and Entry has :: " + entry.getPeg$contentClassification());
			
			List<String> contentFilter = new ArrayList<String>();
			boolean removeCensoredMedia = false;
			boolean removeUnCensoredMedia = false;
			
			if(null == level || level.isEmpty()) {
				contentFilter.add("");
				contentFilter.add(null);
				contentFilter.add("uncensored");
			}else if(null != level && level.equals("censored")){
				contentFilter.add("censored");
				removeCensoredMedia = false;
				removeUnCensoredMedia = true;
			}else if(null != level && level.equals("uncensored")){
				contentFilter.add("censored");
				removeCensoredMedia = true;
				removeUnCensoredMedia = false;
			}
						
			if(contentFilter.contains(entry.getPeg$contentClassification().toLowerCase())) {
				log.info("Level asked is :: " + level + " and It is :: " + entry.getPeg$contentClassification());
				Iterator<Medium> itrMedia = entry.getMedia().iterator();
				if(processMedia) {
					while(itrMedia.hasNext()) {
						Medium medium = itrMedia.next();
						log.info("Medium GUUI IS :: " + medium.getGuid());
						log.info("IF level is censored, then we need to remove GuuId that does end with C.");
						
						if(removeCensoredMedia && null != medium.getGuid() && medium.getGuid().endsWith("C")) {
							itrMedia.remove();
						}else if(removeUnCensoredMedia && null != medium.getGuid() && !medium.getGuid().endsWith("C")){
							itrMedia.remove();
						}
					}					
				}
			}else {
				log.info("Entry needs to be removed for level : " + level + " is :: " + entry.getGuid());
				itr.remove();
			}
		}
		return response;
	}
}
