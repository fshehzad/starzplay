package com.starzplay.factory;
/**
 * 
 * 
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.factory
 * @project starzplay
 *
 */
public class ResponseFilterFactory {
	
	/**
	 * 
	 * @return
	 */
	public static MovieResponseFilter getInstance(String filterType) {
		if(null != filterType && filterType.equals("censoring")) {
			return (new CensoringResponseFilter());	
		}
		return (new CensoringResponseFilter());
	}

}
