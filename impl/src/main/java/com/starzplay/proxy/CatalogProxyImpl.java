package com.starzplay.proxy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.starzplay.common.Constants;
import com.starzplay.dto.autogen.MovieResponse;

import lombok.extern.log4j.Log4j;

/**
 * 
 * 
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.proxy
 * @project starzplay
 *
 */
@Service("catalogProxy")
@Log4j
public class CatalogProxyImpl implements CatalogProxy{
	
	@Autowired
	private RestTemplate restClient;
	
	/**
	 * 
	 */
	public MovieResponse fetch() {
		MovieResponse movieResponse = restClient.getForObject(Constants.MOVIE_CATALOG_URL, MovieResponse.class);		
		return movieResponse;
	}
	
}
