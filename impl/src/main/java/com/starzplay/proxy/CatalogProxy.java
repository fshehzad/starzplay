package com.starzplay.proxy;

import com.starzplay.dto.autogen.MovieResponse;

/**
 * 
 * 
 * Copyright (c) 2018, fahad-shehzad.com All rights reserved.
 * 
 * @author Fahad Shehzad
 * @since Feb 23, 2018
 * @package com.starzplay.proxy
 * @project starzplay
 *
 */
public interface CatalogProxy {

	public MovieResponse fetch();
}
