package com.starzplay.media;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.starzplay.dto.autogen.Entry;
import com.starzplay.dto.autogen.Medium;
import com.starzplay.dto.autogen.MovieResponse;
import com.starzplay.factory.MovieResponseFilter;
import com.starzplay.init.StarplayApplication;
import com.starzplay.proxy.CatalogProxy;
import com.starzplay.services.CatalogService;
import com.starzplay.web.request.MediaRequest;

import lombok.extern.log4j.Log4j;


@RunWith(SpringRunner.class)
@SpringBootTest(classes= {StarplayApplication.class})
public class MediaServiceTest {

	@Value("classpath:catalogproxy/api_mock.json")
    private Resource apiMock;
	
	@Autowired()
	private MovieResponseFilter filterFactory;
	
	@MockBean
	private CatalogProxy catalogProxy;
	
	@Autowired
	private CatalogService catalogService;	
	
	@Before
	public void setUp() throws Exception{
		
	}
	
	
	@Test
	public void injectionSuccessfullTest() {
		assertNotNull(filterFactory);
		assertNotNull(catalogProxy);
	}

	/**
	 * 
	 * @return
	 */
	private MovieResponse getMockMovieResponse() {
		MovieResponse movieResponse = null;		
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		try {
			movieResponse = mapper.readValue(apiMock.getFile(), MovieResponse.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return movieResponse;
	}
	
	@Test
	public void mockApiTest() {
		MovieResponse movieResponse = getMockMovieResponse();
		Mockito.when(catalogProxy.fetch()).thenReturn(movieResponse);
		
		MediaRequest form = new MediaRequest("censoring", null);
		MovieResponse result = catalogProxy.fetch();
		
		assertNotNull("Based on Params Resutl cannot be Empty", movieResponse);
		assertEquals("No Filtering is performed, Hence Entry Size must match",movieResponse.getEntries().size(), result.getEntries().size());
		
		Iterator<Entry> itrEntry = result.getEntries().iterator();
		while(itrEntry.hasNext()) {
			Entry entry = itrEntry.next();
			if(entry.getGuid().equals("OZTHEGREATANDPOWERFULY2013MFR")) {
				performAssert(entry, 1);
				assertTrue("Media count for GUUID :: " + entry.getGuid() + " should be 1 but found is " + entry.getMedia().size(), entry.getMedia().size() == 1);
			}else if(entry.getGuid().equals("VUALTOTESTEXTRA")) {
				performAssert(entry, 1);
				assertTrue("Media count for GUUID :: " + entry.getGuid() + " should be 1 but found is " + entry.getMedia().size(), entry.getMedia().size() == 1);
			}else if(entry.getGuid().equals("HOWTOTRAINYOURDRAGONY2010M")) {
				performAssert(entry, 1);
				assertTrue("Media count for GUUID :: " + entry.getGuid() + " should be 1 but found is " + entry.getMedia().size(), entry.getMedia().size() == 1);
			}else if(entry.getGuid().equals("THINKLIKEAMANY2012M")) {
				performAssert(entry, 2);
				assertTrue("Media count for GUUID :: " + entry.getGuid() + " should be 2 but found is " + entry.getMedia().size(), entry.getMedia().size() == 2);
			}
		}
	}
	
	private void performAssert(Entry entry, int mediaCount) {	
		assertTrue(entry.getGuid() + " -> Media Count must be = "+mediaCount+" but found is :: " + entry.getMedia().size(), entry.getMedia().size() == mediaCount);
	}
	
	@Test
	public void processLevelAsNullOrEmptyTest() {
		MovieResponse movieResponse = getMockMovieResponse();
		Mockito.when(catalogProxy.fetch()).thenReturn(movieResponse);
		
		MediaRequest form = new MediaRequest("censoring", null);
		MovieResponse result = catalogService.process(form);
		
		assertTrue("Based on Mockup, Entry Size must be 3 and found is " + result.getEntries().size(), result.getEntries().size() == 3 );
		
		Iterator<Entry> itrEntry = result.getEntries().iterator();
		while(itrEntry.hasNext()) {
			Entry entry = itrEntry.next();
			if(entry.getGuid().equals("OZTHEGREATANDPOWERFULY2013MFR")) {
				performAssert(entry, 1);
				assertTrue("Media count for GUUID :: " + entry.getGuid() + " should be 1 but found is " + entry.getMedia().size(), entry.getMedia().size() == 1);
			}else if(entry.getGuid().equals("VUALTOTESTEXTRA")) {
				performAssert(entry, 1);
				assertTrue("Media count for GUUID :: " + entry.getGuid() + " should be 1 but found is " + entry.getMedia().size(), entry.getMedia().size() == 1);
			}else if(entry.getGuid().equals("HOWTOTRAINYOURDRAGONY2010M")) {
				performAssert(entry, 1);
				assertTrue("Media count for GUUID :: " + entry.getGuid() + " should be 1 but found is " + entry.getMedia().size(), entry.getMedia().size() == 1);
			}else if(entry.getGuid().equals("THINKLIKEAMANY2012M")) {
				performAssert(entry, 2);
				assertTrue("Media count for GUUID :: " + entry.getGuid() + " should be 2 but found is " + entry.getMedia().size(), entry.getMedia().size() == 2);
			}
		}
	}

	@Test
	public void processLevelAsCensored() {
		MovieResponse movieResponse = getMockMovieResponse();
		Mockito.when(catalogProxy.fetch()).thenReturn(movieResponse);
		
		MediaRequest form = new MediaRequest("censoring", "censored");
		MovieResponse result = catalogService.process(form);
		
		assertTrue("Based on Mockup for Censored, Entry Size must be 1", result.getEntries().size() == 1 );

		assertNotNull("Based on Params Result cannot be Empty", movieResponse);
		assertTrue("Based on Mockup for level = censored, there should be Only 1 Entry.", result.getEntries().size() == Integer.valueOf(1));
		
		Iterator<Entry> itrEntry = result.getEntries().iterator();
		while(itrEntry.hasNext()) {
			Entry entry = itrEntry.next();
			assertTrue("Only one entry with GUUI :: THINKLIKEAMANY2012M should found for censored Level", entry.getGuid().equals("THINKLIKEAMANY2012M"));
			assertTrue("Media count for GUUID :: " + entry.getGuid() + " should be 1 but found is " + entry.getMedia().size(), entry.getMedia().size() == 1);
			
			
			Iterator<Medium> mediaItr = entry.getMedia().iterator();
			while(mediaItr.hasNext()) {
				Medium media = mediaItr.next();
				assertTrue("GUUI SHOULD awalsy be THINKLIKEAMANY2012MC but found :: "+ media.getGuid() , media.getGuid().equals("THINKLIKEAMANY2012MC") );
			}
		}
		
	}

	@Test
	public void processLevelAsUnCensored() {
		MovieResponse movieResponse = getMockMovieResponse();
		Mockito.when(catalogProxy.fetch()).thenReturn(movieResponse);
		
		MediaRequest form = new MediaRequest("censoring", "uncensored");
		MovieResponse result = catalogService.process(form);
		
		assertTrue("Based on Mockup for uncensored, Entry Size must be 1", result.getEntries().size() == 1 );
		
		assertNotNull("Based on Params Resutl cannot be Empty", movieResponse);
		assertTrue("Based on Mockup for level = uncensored, there should be Only 1 Entry which is not the case. Count is :: "+result.getEntries().size(), result.getEntries().size() == Integer.valueOf(1));
		
		Iterator<Entry> itrEntry = result.getEntries().iterator();
		
		while(itrEntry.hasNext()) {
			Entry entry = itrEntry.next();
			assertTrue("Only one entry with GUUI :: THINKLIKEAMANY2012M should found for censored Level", entry.getGuid().equals("THINKLIKEAMANY2012M"));
			assertTrue("Media count for GUUID :: " + entry.getGuid() + " should be 1 but found is " + entry.getMedia().size(), entry.getMedia().size() == 1);
			
			Iterator<Medium> mediaItr = entry.getMedia().iterator();
			while(mediaItr.hasNext()) {
				Medium media = mediaItr.next();
				assertTrue("GUUI SHOULD awalsy be THINKLIKEAMANY2012M but found :: "+ media.getGuid() , media.getGuid().equals("THINKLIKEAMANY2012M") );
			}
		}
	}
}
